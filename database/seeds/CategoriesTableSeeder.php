<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 1; $i <= 10; $i++) {
            $rootNode = Category::create(['name' => $faker->country()]);
            $rootNode->save();

            for ($j = 1; $j <= 9; $j++) {
                $rootNode->children()->create(['name' => $faker->city()]);
                $rootNode->save();
            }
        };
    }
}
